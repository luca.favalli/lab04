import org.gradle.api.Plugin
import org.gradle.api.Project
import static groovy.io.FileType.FILES

class EiffelPlugin implements Plugin<Project> {
    String file;
    
    @Override
    void apply(Project project) {
        project.extensions.create("eiffelCompile", EiffelDirPluginExtension)
        def String root = "" + project.rootProject.projectDir + '/' + project.eiffelCompile.root;
        
        project.task('eiffel_compile').doLast{
            project.exec {
                commandLine 'ec', '-config', root + project.eiffelCompile.path + project.eiffelCompile.file + '.ecf';
            }

        }
        
        project.task('eiffel_finish_freezing').doLast {
            project.exec {
                commandLine 'finish_freezing', '-location', root + project.eiffelCompile.generated + project.eiffelCompile.file + '/W_code';
            }
        }
        
        project.tasks.getByName('eiffel_finish_freezing').dependsOn(project.tasks.getByName('eiffel_compile'));
    }
}


class EiffelDirPluginExtension {
    def String root = 'eiffel/'
    def String path = 'src/';
    def String generated = 'EIFGENs/'
    def String file;
}
